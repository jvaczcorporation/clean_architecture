import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_auth_platform_interface/firebase_auth_platform_interface.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:clean_architecture/app/modules/login/domain/entities/logged_user.dart';
import 'package:clean_architecture/app/modules/login/domain/errors/errors.dart';
import 'package:clean_architecture/app/modules/login/external/datasources/firebase_datasource_login.dart';
import 'package:mockito/mockito.dart';


class FirebaseUserMock extends Mock implements User {}

class AuthResultMock extends Mock implements UserCredential {}

class AuthCredentialMock extends Mock implements AuthCredential {}

class OAuthCredentialMock extends Mock implements OAuthCredential {}

class AuthExceptionMock extends Mock implements FirebaseAuthException {}

class FirebaseAuthMock extends Mock implements FirebaseAuth {}

class GoogleSignInMock extends Mock implements GoogleSignIn {}

class GoogleSignInAuthenticationMock extends Mock implements GoogleSignInAuthentication {}

class GoogleSignInAccountMock extends Mock implements GoogleSignInAccount {}

final credential = AuthCredentialMock();
final authException = AuthExceptionMock();
final signIngAccount = GoogleSignInAccountMock();
final signInAuthentication = GoogleSignInAuthenticationMock();
final oAuthCredential = OAuthCredentialMock();

main() {
  final auth = FirebaseAuthMock();
  final firebaseUser = FirebaseUserMock();
  final user = const LoggedUser(
    uid: "uid",
    name: "teste",
    phoneNumber: "123456",
    email: "teste@teste.com",
  );

  final googleSignIn = GoogleSignInMock();
  final authResult = AuthResultMock();
  final datasource = FirebaseDataSourceLoginImpl(auth, googleSignIn);

  setUpAll(() {
    when(firebaseUser.uid).thenReturn("uid");
    when(firebaseUser.displayName).thenReturn("teste");
    when(firebaseUser.email).thenReturn("teste@teste.com");
    when(firebaseUser.phoneNumber).thenReturn("123456");
    when(authResult.user).thenReturn(firebaseUser);

    when(auth.signInWithEmailAndPassword(email: anyNamed('email'), password: anyNamed('password'))).thenAnswer((_) async => authResult);
    when(auth.signInWithCredential(any)).thenAnswer((_) async => authResult);
  });

  group("login Google", (){
    test('should return Logged User loginGoogle', () async {
      when(googleSignIn.signIn()).thenAnswer((_) async => signIngAccount);
      when(signIngAccount.authentication).thenAnswer((_) async => signInAuthentication);
      when(signInAuthentication.idToken).thenAnswer((_) => "idToken");
      when(signInAuthentication.accessToken).thenAnswer((_) => "accessToken");

      var result = await datasource.loginGoogle();
      expect(result.uid, equals(user.uid));
      expect(result.name, equals(user.name));
      expect(result.phoneNumber, equals(user.phoneNumber));
      expect(result.email, equals(user.email));
    });

    test('should return Logged Failed CancelledByUser', () async {
      when(googleSignIn.signIn()).thenAnswer((_) async => null);
      var result = datasource.loginGoogle();
      expect(result, throwsA(isA<ErrorCancelledByUser>()));
    });
  });

  group("logged User", (){
    test('should return Logged User', () async {
      when(auth.currentUser).thenAnswer((_) => firebaseUser);
      var result = await datasource.currentUser();
      expect(result.name, equals(user.name));
      expect(result.phoneNumber, equals(user.phoneNumber));
      expect(result.email, equals(user.email));
    });
    test('should return ErrorGetLoggedUser if User is not logged', () async {
      when(auth.currentUser).thenAnswer((_) => null);
      expect(datasource.currentUser(), throwsA(isA<ErrorGetLoggedUser>()));
    });
  });

  group("logout User", (){
    test('should complete logout', () async {
      when(auth.signOut()).thenAnswer((_) async {});
      expect(datasource.logout(), completes);
    });
    test('should return error', () async {
      when(auth.signOut()).thenThrow(Exception());
      expect(datasource.logout(), throwsA(isA<Exception>()));
    });
  });

}
