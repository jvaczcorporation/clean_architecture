import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:clean_architecture/app/core/connectivity/domain/services/connectivity_service.dart';
import 'package:clean_architecture/app/core/connectivity/domain/errors/errors.dart';
import 'package:clean_architecture/app/modules/login/domain/repositories/login_repository.dart';
import 'package:clean_architecture/app/modules/login/domain/usecases/login_with_google.dart';
import 'package:clean_architecture/app/modules/login/infra/models/user_model.dart';
import 'package:mockito/mockito.dart';

class LoginRepositoryMock extends Mock implements LoginRepository {}

class ConnectivityServiceMock extends Mock implements ConnectivityService {}

class FirebaseUserMock extends Mock implements User {}

main() {
  final repository = LoginRepositoryMock();
  final service = ConnectivityServiceMock();
  final usecase = LoginWithGoogleImpl(repository, service);

  setUpAll(() {
    when(service.isOnline()).thenAnswer((_) async => Right(unit));
  });

  test('should consume repository loginGoogle', () async {
    final user = UserModel(name: "teste", email: "teste@teste.com");
    when(repository.loginGoogle())
        .thenAnswer((_) async => Right(user));
    final result = await usecase();

    expect(result, Right(user));
    expect(result.fold(id, (r) => user.name), "teste");
    expect(result.fold(id, (r) => user.email), "teste@teste.com");
  });

  test('should return error when offline', () async {
    when(service.isOnline()).thenAnswer((_) async => Left(ErrorConnection()));

    final result = await usecase();
    expect(result.leftMap((l) => l is ErrorConnection), Left(true));
  });
}
