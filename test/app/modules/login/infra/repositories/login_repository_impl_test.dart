import 'package:clean_architecture/app/modules/login/domain/entities/logged_user_info.dart';
import 'package:clean_architecture/app/modules/login/domain/errors/errors.dart';
import 'package:clean_architecture/app/modules/login/infra/datasources/login_datasource.dart';
import 'package:clean_architecture/app/modules/login/infra/models/user_model.dart';
import 'package:clean_architecture/app/modules/login/infra/repositories/login_repository_impl.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';

class LoginDataSourceMock extends Mock implements LoginDataSource {}

main() {
  final datasource = LoginDataSourceMock();
  final userReturn = UserModel(
    name: "teste",
    email: "teste@gmail.com",
    phoneNumber: "1234567",
  );
  final repository = LoginRepositoryImpl(datasource);

  group("loginGoogle", () {
    test('should get UserModel', () async {
      when(datasource.loginGoogle()).thenAnswer((_) async => userReturn);
      var result = await repository.loginGoogle();
      expect(result, isA<Right<dynamic, LoggedUserInfo>>());
    });
    test('should call ErrorCancelledByUser', () async {
      when(datasource.loginGoogle()).thenThrow(ErrorCancelledByUser());
      var result = await repository.loginGoogle();
      expect(result.leftMap((l) => l is ErrorCancelledByUser), Left(true));
    });
    test('should call ErrorLoginEmail', () async {
      when(datasource.loginGoogle()).thenThrow(ErrorLoginGoogle());
      var result = await repository.loginGoogle();
      expect(result.leftMap((l) => l is ErrorLoginGoogle), Left(true));
    });
  });

  group("loggedUser", () {
    test('should get Current User Logged', () async {
      when(datasource.currentUser()).thenAnswer((_) async => userReturn);
      var result = await repository.loggedUser();
      expect(result, isA<Right<dynamic, LoggedUserInfo>>());
    });
    test('should Throw when user not logged', () async {
      when(datasource.currentUser()).thenThrow(ErrorGetLoggedUser());
      var result = await repository.loggedUser();
      expect(result.leftMap((l) => l is ErrorGetLoggedUser), Left(true));
    });
  });

  group("logout", () {
    test('should get logout', () async {
      when(datasource.logout()).thenAnswer((_) async {});
      var result = await repository.logout();
      expect(result, isA<Right<dynamic, Unit>>());
    });
    test('should Throw when user try logout', () async {
      when(datasource.logout()).thenThrow(ErrorGetLoggedUser());
      var result = await repository.logout();
      expect(result.leftMap((l) => l is ErrorLogout), Left(true));
    });
  });
}
